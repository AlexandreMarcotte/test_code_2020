import pyqtgraph.opengl as gl
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
from random import random
import cv2


app = QtGui.QApplication([])
view = gl.GLViewWidget()
view.setCameraPosition(distance=120, azimuth=-90)
view.show()


def create_random_array():
    arr = np.zeros((50, 50, 1, 4))
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            r = random() * 255
            g = random() * 255
            b = random() * 255
            arr[i, j, 0] = [r, g, b, 255]
    return arr


def create_3d_volume_from_3d_np_array(arr, scale=1, translate=(0, 0, 0)):
    volume = gl.GLVolumeItem(arr, sliceDensity=3, smooth=False)
    # Translate
    volume.translate(-arr.shape[0]/2 * scale,
                     -arr.shape[1]/2 * scale,
                     -arr.shape[2]/2 * scale)
    volume.translate(*translate)
    # Scale
    # v.scale(scale, scale, scale)
    return volume


def load_img_as_array():
    img_path = '/home/record/Documents/CODING/test_code_2019/test_code/3D/open_gl/cat.png'
    img = cv2.imread(img_path)
    alpha = 100

    img_rgba = np.concatenate(
        (img, np.ones((img.shape[0], img.shape[1], 1)) * alpha), axis=2)

    img_rgba = img_rgba.reshape((img_rgba.shape[0], img_rgba.shape[1], 1, 4))

    return img_rgba


def main():
    img_rgba = load_img_as_array()
    # img_rgba = create_random_array()

    view.addItem(create_3d_volume_from_3d_np_array(img_rgba))

    QtGui.QApplication.instance().exec_()


if __name__ == '__main__':
    main()


