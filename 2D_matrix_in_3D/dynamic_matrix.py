import pyqtgraph.opengl as gl
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
from random import random
from functools import partial

import cv2

app = QtGui.QApplication([])
view = gl.GLViewWidget()
view.setCameraPosition(distance=120, azimuth=-90)
view.show()


def create_random_array():
    arr = np.zeros((50, 50, 1, 4))
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            r = random() * 255
            g = random() * 255
            b = random() * 255
            arr[i, j, 0] = [r, g, b, 255]
    return arr


def load_img_as_array(img_no):
    img_no = '000' + str(img_no%90 + 190)
    img_path = f'/home/record/Desktop/faceswap_rogan_musk/kauff_hoodie/img/kauffman_hoodie_{img_no}_0.png'
    img = cv2.imread(img_path)
    # Cv2 use BGR so convert to RGB
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    alpha = 255

    img_rgba = np.concatenate(
        (img, np.ones((img.shape[0], img.shape[1], 1)) * alpha), axis=2)

    img_rgba = img_rgba.reshape((img_rgba.shape[0], img_rgba.shape[1], 1, 4))

    return img_rgba


def create_3d_volume_from_3d_np_array(arr, i=0, j=0, k=0, scale=1):
    volume = gl.GLVolumeItem(arr, sliceDensity=1, smooth=False)
    # Translate
    volume.translate(-arr.shape[0] * scale, -arr.shape[0]/2 * scale, 0)
    volume.translate(70*i, 70*j, 70*k)
    # Scale
    # v.scale(scale, scale, scale)
    return volume


class Updater:
    def __init__(self):
        self.img_no = 0

    def update(self):
        self.img_no += 1
        # arr = create_random_array()
        arr = load_img_as_array(self.img_no)
        # Remove all items
        view.items = []

        for i in range(1):
            for j in range(1):
                for k in range(1):
                    view.addItem(create_3d_volume_from_3d_np_array(arr, i, j, k))
       
        view.addItem(create_3d_volume_from_3d_np_array(arr))


def main():
    updater = Updater()
    t = QtCore.QTimer()
    t.timeout.connect(updater.update)
    t.start(80)
    QtGui.QApplication.instance().exec_()


if __name__ == '__main__':
    main()



