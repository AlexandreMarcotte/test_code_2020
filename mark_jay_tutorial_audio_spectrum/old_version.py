"""
# how many audio per frame displayed
CHUNK = 1024 * 4  # number of sample per chunk
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100  # K hertz
p = pyaudio.PyAudio()
stream = p.open(
    format=FORMAT, channels=CHANNELS, rate=RATE, input=True,
    output=True, frames_per_buffer=CHUNK)


# PLOT 1:
fig, ax = plt.subplots()

x = np.arange(0, 2 * CHUNK, 2)
line, = ax.plot(x, np.random.rand(CHUNK))
ax.set_ylim(-10000, 10000)
ax.set_xlim(0, CHUNK)

while True:
    print('in')
    data = stream.read(CHUNK)
    data_int = struct.unpack(str(CHUNK) + 'h', data)
    line.set_ydata(data_int)
    fig.canvas.draw()
    fig.show()
    fig.canvas.flush_events()

"""
# data = stream.read(CHUNK)
# data_int = struct.unpack(str(CHUNK) + 'h', data)
