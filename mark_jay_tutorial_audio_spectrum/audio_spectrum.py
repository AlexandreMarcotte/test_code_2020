import pyaudio
import struct
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from collections import deque


class LiveGraph:
    def __init__(self):
        # --Audio--
        # how many audio per frame displayed
        self.CHUNK = 1024 * 4  # number of sample per chunk
        FORMAT = pyaudio.paInt16
        CHANNELS = 1
        RATE = 44100  # K hertz
        p = pyaudio.PyAudio()
        self.stream = p.open(
            format=FORMAT, channels=CHANNELS, rate=RATE, input=True,
            output=True, frames_per_buffer=self.CHUNK)
        # --graph--
        self.win = pg.GraphicsWindow()
        self.p = self.win.addPlot()
        self.p.setYRange(-10000, 10000)
        MAX_LEN = self.CHUNK * 10
        self.data = deque(np.zeros(MAX_LEN), maxlen=MAX_LEN)
        self.curve1 = self.p.plot(self.data)
        self.ptr = 0

    def start_timer(self):
        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(0)

    def update(self):
        data = self.stream.read(self.CHUNK)
        data_int = struct.unpack(str(self.CHUNK) + 'h', data)
        for d in data_int:
            self.data.append(d)
        self.curve1.setData(self.data)


if __name__ == '__main__':
        live_graph = LiveGraph()
        live_graph.start_timer()

        QtGui.QApplication.instance().exec_()




