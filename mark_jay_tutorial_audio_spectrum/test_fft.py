import numpy as np
from scipy import fft
import matplotlib.pyplot as plt
from math import pi


CHUNK = 500  # Sampling in Hz
t = np.arange(0, CHUNK)
print(t)
omega = 0.1 * 2 * pi
signal = np.sin(omega * t)
fft = np.abs(fft(signal))
# keep only the first half of the fft because second half is the mirror image in the imaginary domain
fft = fft[:len(fft)//2]

# plot
plt.plot(signal)
plt.show()
freq_range = np.linspace(0, 1, CHUNK//2)
plt.plot(freq_range, fft)
plt.show()
