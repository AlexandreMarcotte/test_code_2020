# See eeg plot creator
import pyaudio
import struct
import numpy as np
import pyqtgraph as pg
from collections import deque
from PyQt5.QtWidgets import QWidget, QGridLayout, QMainWindow, QApplication, QTabWidget
import sys
import time

#%%

class FftCalculator:
    def __init__(self):
        self.t = None

    def get_freq_range(self, n_data):
        """Calculate FFT (Remove freq 0 because it gives a really high
         value on the graph"""
        return np.linspace(0, n_data // 2 / self.get_delta_t(), n_data // 2)

    def calcul_fft(self, queue):
        fft = np.fft.fft(queue)
        return abs(fft[:len(queue) // 2])

    def get_fft_to_plot(self, queue):
        fft = self.calcul_fft(queue)
        f_range = self.get_freq_range(len(queue))
        # Use as: curve.setData(f_range, fft)
        return f_range, fft

    def get_delta_t(self):
        """interval of time from the first to the last value that was
        add to the queue"""
        return self.gv.t_queue[self.gv.filter_max_bound - 1] \
               - self.gv.t_queue[self.gv.filter_min_bound]


def calculate_fft(queue):
    fft = np.fft.fft(queue)
    return abs(fft[:len(queue) // 2])


class AudioStreamer:
    def __init__(self):
        # how many audio per frame displayed
        self.CHUNK = 1024 * 2  # number of sample per chunk
        self.data_int = np.zeros(self.CHUNK)
        FORMAT = pyaudio.paInt16
        CHANNELS = 1
        self.RATE = 44100  # Khz
        p = pyaudio.PyAudio()
        self.stream = p.open(
            format=FORMAT, channels=CHANNELS, rate=self.RATE, input=True,
            output=True, frames_per_buffer=self.CHUNK)
        self.init_time = time.time()
        self.time = deque(maxlen=5)

    def update_data_int(self):
        self.time.append(time.time() - self.init_time )
        print(self.time)
        data = self.stream.read(self.CHUNK)
        # Update the self.data_int value
        self.data_int = struct.unpack(str(self.CHUNK) + 'h', data)
        return self.data_int


class FFTPlotWidget(QWidget):
    def __init__(self):
        super().__init__()


class TimeDomainPlotWidget(QWidget):
    def __init__(self, streamer):
        super().__init__()
        # Audio streamer
        self.streamer = streamer
        # data
        MAX_LEN = self.streamer.CHUNK * 40
        self.data = deque(np.zeros(MAX_LEN), maxlen=MAX_LEN)

        self.layout = QGridLayout(self)

        self.time_domain_curve = self._create_curve(_range=(-10000, 10000))
        self.fft_curve = self._create_curve(_range=(0, 20000))

        self.start_timer()

    def _create_curve(self, _range: tuple):
        p = pg.PlotWidget()
        p.setYRange(*_range)
        self.layout.addWidget(p)
        return p.plot(self.data)

    def start_timer(self):
        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(0)

    def update(self):
        # This line of code will update the data_int value
        data_int = self.streamer.update_data_int()
        # So that in this next line we can go through the values
        for d in data_int:
            self.data.append(d)

        # Time domain
        self.time_domain_curve.setData(self.data)
        # FFT domain
        self.fft_curve.setData(calculate_fft(self.data))


"""
class TabWidget(QTabWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.audio_streamer = AudioStreamer()
        self.tabs_list = {
                TimeDomainPlotWidget(self.audio_streamer): 'Time Domain Plot'}
                # TimeDomainPlotWidget(self.audio_streamer): 'Time Domain Plot'}

        for tab, name in self.tabs_list.items():
            self.addTab(tab, name)
"""


class LiveGraphMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # self.tab_w = TabWidget(self)
        self.audio_streamer = AudioStreamer()
        time_domaine_plot_widget = TimeDomainPlotWidget(self.audio_streamer)
        self.setCentralWidget(time_domaine_plot_widget)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    live_graph = LiveGraphMainWindow()
    live_graph.show()

    sys.exit(app.exec_())
