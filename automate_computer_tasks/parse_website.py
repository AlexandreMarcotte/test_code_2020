import urllib3
from bs4 import BeautifulSoup
import requests
import re
from time import sleep


for i in range(100):
    # From :   https://www.freecodecamp.org/news/how-to-scrape-websites-with-python-and-beautifulsoup-5946935d93fe/
    url = 'https://pixelplanet.fun/#d,0,0,16'
    # query the website and return the html to the variable 'page'
    response = requests.get(url)
    html = response.content
    # parse the html using beautiful soup and store in variable soup
    soup = BeautifulSoup(html, features='html.parser')
    print(soup.prettify())
    # find the position with regex
    text = soup.find_all(text=True)
    pos = re.findall(r'[-+]?\d+\.\d+|[-+]?\d+', text[-1])
    print(pos)
    print('')
    sleep(0.5)





# x_coord = soup.find_all('script')
# print(x_coord[2])
# for val in x_coord:
#     print(val)
# soup_pretty = soup.prettify()
# print(soup_pretty)