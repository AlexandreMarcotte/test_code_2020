from time import sleep
import pyautogui
pyautogui.FAILSAFE= True
import numpy as np
import cv2
import time
import pytesseract
from PIL import Image
import sys
import pyperclip
import matplotlib.pyplot as plt
import re
from math import isclose
import itertools
import random


def get_current_pos():
    pyautogui.click(100, 1040)
    print('click')
    art_pos = pyperclip.paste()
    art_pos = eval(art_pos[3:])
    print('art pos', art_pos)
    return art_pos


def get_ocr_number(region=(38, 1033, 112, 23), how_many_num=2, show_img_verification=False):
    number = None
    img_pos = pyautogui.screenshot(region=region)
    # Show image for position verification
    if show_img_verification:
        plt.imshow(img_pos)
        plt.show(block=False)
        plt.pause(2)
        plt.close()
    text_pos = pytesseract.image_to_string(img_pos)
    print('text pos: ', text_pos)
    try:
        try:
            if how_many_num == 2:
                number = re.findall('[-+]?\d+', text_pos)
                number = (int(number[0]), int(number[1]))
            else:
                print(text_pos)
                number = re.findall('\d+', text_pos)
                print(number)
                try:
                    if len(number) > 0:
                        number = int(number[0])
                    else:
                        number = 0
                except TypeError:
                    number = 0

        except NameError:
            pass
    except SyntaxError:
        pass
    return number

    # summarize some details about the image


def get_map_coordinates(screen_location):
    sleep(0.2)
    pyautogui.moveTo(*screen_location)
    sleep(0.2)
    corner_1 = get_ocr_number()
    return corner_1


def calculate_vectors(corners):
    vectors = []

    # (corner start vector, corner finish vector)
    for cornerA, cornerB in [(0, 1), (0, 2), (1, 2)]:
        vect_computer = calculate_vector(corners[cornerA].computer, corners[cornerB].computer)
        vect_map = calculate_vector(corners[cornerA].map, corners[cornerB].map)
        vectors.append(Vector(vect_computer, vect_map))

    return vectors


def calculate_vector(pt1, pt2):
    return (pt2[0]-pt1[0], pt2[1]-pt1[1])


class Corner:
    def __init__(self, computer_corner):
        self.computer = computer_corner
        self.map = None

    def __str__(self):
        return f'computer corner: {self.computer} | map corner: {self.map}'


class Vector:
    def __init__(self, computer_corner, map_corner):
        self.computer = computer_corner
        self.map = map_corner
        self.ratio = self.calculate_ratio()

    def calculate_ratio(self):
        return (self.computer[0]/self.map[0], self.computer[1]/self.map[1])

    def __str__(self):
        return f'computer vector: {self.computer} | map vector: {self.map}'


def validate_vector_ratio(vectors):
    validation = True
    # Create the possible combinations to evaluate if almost equal
    combinations = list(itertools.combinations(range(len(vectors)), 2))

    for ratio_pos in range(2):
        for c1, c2 in combinations:
            if not isclose(vectors[c1].ratio[ratio_pos], vectors[c2].ratio[ratio_pos],
                       abs_tol=0.1*vectors[c1].map[ratio_pos]):
                validation = False
    print('Validation: ', validation)
    return validation


def average_vector_ratio(vectors):
    ratio_x = 0
    ratio_y = 0
    for v in vectors:
        ratio_x += v.ratio[0]
        ratio_y += v.ratio[1]
    return ratio_x/len(vectors), ratio_y/len(vectors)


def calibrate_w_3_corner():
    corners = [Corner((248, 164)), Corner((1734, 460)), Corner((842, 970))]

    for corner in corners:
        corner.map = get_map_coordinates(screen_location=corner.computer)
        print(corner)

    vectors = calculate_vectors(corners)
    validate_vector_ratio(vectors)
    for vector in vectors:
        print('ratio : ', vector.ratio)

    ratio = average_vector_ratio(vectors)
    return ratio


def test_calibration_accuracy():
    previous_pos = pyautogui.position()
    art_init_pos = np.array(get_ocr_number())
    motion_vector = np.array([0, 0])
    while True:
        new_pos = pyautogui.position()
        motion_vector += np.array(calculate_vector(previous_pos, new_pos))
        previous_pos = new_pos
        print(motion_vector)
        art_pos = art_init_pos + motion_vector/calibration_ratio
        print('art pos ', art_pos)


def verify_sleep():
    verify_button = pyautogui.locateCenterOnScreen('verify_button.png')
    print('Verify_button: ', verify_button)
    if verify_button:
        print('here1')
        sleep(2)
        while pyautogui.locateCenterOnScreen('verify_button.png'):
            print('here2')
            sleep(30)


if __name__ == '__main__':
    # while True:
    #     print(pyautogui.position())
    #     sleep(5)
    calibration_ratio = calibrate_w_3_corner()
    calibration_ratio = (calibration_ratio[0]*0.97, calibration_ratio[1]*0.97)
    # calibration_ratio = [3, 3]
    # Decrease a bit calibration ratio for rounding errors?
    print('calibration ratio: ', calibration_ratio)

    # Read img into a numpy array
    lbry_logo = cv2.imread('/home/record/Videos/LBRY_video/pixel_art/lbry_logo.jpg')
    lbry_logo = cv2.cvtColor(lbry_logo, cv2.COLOR_BGR2GRAY)
    print('SHAPE: ', lbry_logo.shape)
    sleep(2)

    # Set the start point:
    init_computer_pos = (100, 200)
    pyautogui.moveTo(init_computer_pos)
    init_art_pos = get_ocr_number()
    # init_art_pos = (10000, 10000)

    x = lbry_logo.shape[1]-1
    print('x', x)
    y = lbry_logo.shape[0]-1
    print('y', y)
    for i in range(80, x):
        for j in range(y):
            if lbry_logo[j][i] > 100:
                # WAIT
                time_to_wait = get_ocr_number(region=(940, 133, 40, 20), how_many_num=1)
                print('time to wait: ', time_to_wait)
                if time_to_wait > 50:
                    print('sleeeeep')
                    sleep(time_to_wait - 3)


                # BOT VERIFICATION:
                verify_sleep()
                pyautogui.moveTo(
                    init_computer_pos[0] + i * calibration_ratio[0],
                    init_computer_pos[1] + j * calibration_ratio[1])
                pyautogui.click()
                verify_sleep()
            print(lbry_logo[j][i], 'pos: i ', i,' j ', j)


    # plt.imshow(lbry_logo)
    # plt.show()




