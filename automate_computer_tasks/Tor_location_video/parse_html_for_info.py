from bs4 import BeautifulSoup
import csv


def find_ip_address():
    html_path = '/home/record/.local/share/torbrowser/tbb/x86_64/tor-browser_en-US/Browser/Downloads/IP Location Finder - Geolocation.html'
    with open(html_path) as f:
        soup = BeautifulSoup(f, 'html.parser')

    table = soup.find_all('table')[0]
    table_val = table.find_all('td')

    # for v in table_val:
    #     print(v)
    #     print('---')

    # Extract pertinent information
    IP = table_val[0].string
    country = table_val[1]
    city = table_val[2].string
    lat = float(table_val[-2].string)
    long = float(table_val[-1].string)
    # Print extracted information
    print('IP: ', IP, ' Country: ', country, ' City: ', city)
    print('Latitude: ', lat, ' Longitude: ', long)
    print('--------------------------------------')
    # Append data to file
    with open('lat_long.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow([IP, lat, long])


if __name__ == '__main__':
    find_ip_address()

