import folium
import sys
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtWebEngineWidgets
import pandas as pd

# Create a Map instance
m = folium.Map(
    location=[47.5088, -75.554],
    # tiles='Stamen Terrain',
    tiles='OpenStreetMap',
    zoom_start=1, control_scale=True)

# Open data
with open('/home/record/Documents/CODING/test_code_2020/automate_computer_tasks/Tor_location_video/lat_long.csv', 'r') as f:
    df = pd.read_csv(f, header=None)
    for index, row in df.iterrows():
        print(row[1], row[2])
        # CircleMarker with radius
        folium.CircleMarker(location=[row[1], row[2]],
                            radius=2).add_to(m)

# Marker
# folium.Marker([45.5088, -73.554], popup='London Bridge').add_to(m)

m.save('my_map.html')

app = QApplication(sys.argv)
view = QtWebEngineWidgets.QWebEngineView()

view.load(QtCore.QUrl().fromLocalFile(
    '/home/record/Documents/CODING/test_code_2020/maps/my_map.html'))

view.show()
sys.exit(app.exec_())
