from time import sleep
from pyautogui import hotkey
import pyautogui


def open_tor_browser():
    sleep(0.5)
    hotkey('winleft')
    sleep(1)
    pyautogui.typewrite('tor')
    hotkey('enter')
    sleep(1)


def enter_website():
    hotkey('ctrl', 'l')
    sleep(0.1)
    pyautogui.typewrite('iplocation.net/', interval=0.05)
    sleep(0.1)
    hotkey('enter')
    wait_till_loaded('ip_location_logo.png')


def wait_till_loaded(img):
    # Wait till the website is loaded
    on_ip_location_website = False
    # Wait till we see the logo of the website which means that the website is loaded
    while not on_ip_location_website:
        sleep(0.2)
        on_ip_location_website = pyautogui.locateCenterOnScreen(img)
        # print('on_ip_location_website: ', on_ip_location_website)
        # pyautogui.screenshot(f'my_screen{i}.png')


def restart_tor_session():
    loc = pyautogui.locateCenterOnScreen('restart_btn.png')
    pyautogui.click(loc)
    wait_till_loaded('duckduckgo_logo.png')


def save_html():
    hotkey('ctrl', 's')
    # To go in the save box
    hotkey('enter')
    # To replace the existing file
    wait_till_loaded('replace_screen.png')
    hotkey('enter')
    # Wait for it to save
    sleep(7)


def pull_html_from_ip_adress_site_on_tor():
    # on first pass only
    open_tor_browser()
    restart_tor_session()
    enter_website()
    save_html()


if __name__ == '__main__':
    # Test
    pull_html_from_ip_adress_site_on_tor()

